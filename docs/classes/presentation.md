### classes[0] = "Presentation"

#### Laboratório de Bioinformática 2021-2022

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

## Practical info

* &shy;<!-- .element: class="fragment" -->Schedule: Fridays - 09:30 to 13:30, **Room 1.10**
* &shy;<!-- .element: class="fragment" -->Questions? f.pina.martins@estbarreiro.ips.pt
* &shy;<!-- .element: class="fragment" -->[Moodle](http://moodle.ips.pt/)

---

## Avaliação

* &shy;<!-- .element: class="fragment" --><font color="green">Contínua</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">20% - Contexto sala de aula:</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">10% - Atitude</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">5% - Participação</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">5% - Postura/interesse</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">20% - Projeto (grupo)</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">60% - Relatório Final (individual)</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">10% - Relatório individual</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">30% - Trabalho prático</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">20% - Apresentação</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">Sem notas mínimas</font>

---

## How will this work?

* &shy;<!-- .element: class="fragment" -->You will be assigned a (BIG) project
  * &shy;<!-- .element: class="fragment" -->You will have to implement it
  * &shy;<!-- .element: class="fragment" -->Work as a team
  * &shy;<!-- .element: class="fragment" -->My role will be that of a mentor, more than a teacher

|||

## New skills?

* &shy;<!-- .element: class="fragment" -->[Git](https://git-scm.com/)
* &shy;<!-- .element: class="fragment" -->[PEP8](https://www.python.org/dev/peps/pep-0008/)
* &shy;<!-- .element: class="fragment" -->[Continuos integration](https://codeship.com/continuous-integration-essentials) and [Docker](https://docker.com)

---

## Picking a project

* &shy;<!-- .element: class="fragment" -->Remember all you learned up to now
* &shy;<!-- .element: class="fragment" -->Pick something you enjoyed
* &shy;<!-- .element: class="fragment" -->Make it awesome

---

## References

* [Git](https://git-scm.com/)
* [PEP8](https://www.python.org/dev/peps/pep-0008/)
* [Continuos integration](https://codeship.com/continuous-integration-essentials)
* [Docker](https://docker.com)
