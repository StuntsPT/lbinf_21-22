# Assignment 01

## Project

### Deadline:

TBD


### Format:

PDF


### Delivery:

E-mail


### Details:

* Your project must have:
  * Brief introduction to the problem
  * Objectives
  * Main methods
  * Expected results
  * Group members and their tasks
* Approach must be **automated**!


Don't forget a cover and references, of course.
